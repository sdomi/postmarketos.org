# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later
import collections
import json
import logo
import markdown
import os
import re
import yaml

from atom import AtomFeed

from datetime import datetime
from flask import Flask, url_for, Response, request, send_file
from os import listdir

# current dir
import blog
import config
import config.mirrors
import config.wiki
import page
import template


app = Flask(__name__)
app.config['DARK'] = False
app.config['FREEZER_IGNORE_MIMETYPE_WARNINGS'] = True


@app.route('/')
def home():
    return template.render_template('index.html')

@app.route('/robots.txt')
def robots_txt():
    return send_file('static/robots.txt')

@app.route('/.well-known/dnt-policy.txt')
def dnt_policy():
    return send_file('static/dnt-policy.txt')

@app.route('/.well-known/matrix/server')
def matrix_server():
    return send_file('static/matrix-server.json')

@app.route('/logo.svg')
def logo_svg():
    return Response(response=logo.create(phone=False), mimetype="image/svg+xml")

@app.route('/blog/')
def blog_():
    year_posts = blog.get_posts(create_html=False)
    return template.render_template('blog.html', year_posts=year_posts,
                                    blog_name="blog")

@app.route('/blog/feed.atom')
def atom():
    feed = AtomFeed(author='postmarketOS bloggers',
                    feed_url=request.url,
                    icon=url_for('logo_svg', _external=True),
                    title='postmarketOS Blog',
                    url=url_for('blog_', _external=True))

    for year, posts in blog.get_posts(external_links=True).items():
        for post in posts:
            feed.add(content=post['html'],
                     content_type='html',
                     title=post['title'],
                     url=post['url'],
                     # midnight
                     updated=datetime.combine(post['date'],
                                              datetime.min.time()))
    return feed.get_response()

@app.route('/blog/2020/07/21/breaking-update-in-edge/')
def blog_post_redirect_edge():
    # Special post that was created in /blog before /edge was introduced, then
    # moved.
    return template.render_template(
        "redirect.html",
        url="/edge/2020/07/21/breaking-update-in-edge/")


@app.route('/blog/<y>/<m>/<d>/<slug>/')
def blog_post(y, m, d, slug):
    parsed_post = blog.parse_post('-'.join([y, m, d, slug]) + '.md')
    return template.render_template('blog-post.html', **parsed_post)

@app.route('/edge/')
def edge():
    year_posts = blog.get_posts(create_html=False, dir=config.content_dir_edge)
    return template.render_template('blog.html', year_posts=year_posts,
                                    blog_name="edge")

@app.route('/edge/feed.atom')
def edge_atom():
    feed = AtomFeed(author='postmarketOS',
                    feed_url=request.url,
                    icon=url_for('logo_svg', _external=True),
                    title='Breaking updates in pmOS edge',
                    url=url_for('edge', _external=True))

    year_posts = blog.get_posts(external_links=True,
                                dir=config.content_dir_edge)
    for year, posts in year_posts.items():
        for post in posts:
            feed.add(content=post['html'],
                     content_type='html',
                     title=post['title'],
                     url=post['url'],
                     # midnight
                     updated=datetime.combine(post['date'],
                                              datetime.min.time()))
    return feed.get_response()

@app.route('/edge/<y>/<m>/<d>/<slug>/')
def edge_post(y, m, d, slug):
    year_posts = blog.parse_post('-'.join([y, m, d, slug]) + '.md',
                                 dir=config.content_dir_edge)
    return template.render_template('blog-post.html', **year_posts)

@app.route('/move.html')
def static_page_move():
    # Do not redirect /move.html to /move/ (as static_page_redirect() would do)
    # because it is a redirect page already. This would break the JS redirect
    # code in content/page/move.md and we have linked to it in all the github
    # projects by now.
    return static_page_or_wiki_redirect("move")


@app.route('/mirrors.json')
def mirrors_json():
    return json.dumps(config.mirrors.mirrors, indent=4)


@app.route('/<page>.html')
def static_page_redirect(page):
    # Pages in /content/page/ used to be at postmarketos.org/<page>.html
    return template.render_template("redirect.html", url=f"/{page}/")


@app.route('/<slug>/')
def static_page_or_wiki_redirect(slug):
    """ WARNING: This must be the last route! """
    page_path = os.path.join(config.content_dir_page, f"{slug}.md")

    # Avoid MissingURLGeneratorWarning
    if slug == "static":
        return template.render_template("redirect.html", url="/")

    # Wiki redirect
    if not os.path.exists(page_path):
        wiki_url = ("https://wiki.postmarketos.org/wiki/"
                    f"{config.wiki.redirects[slug]}")
        return template.render_template('redirect.html', url=wiki_url)

    # Page from content/page/*.md
    data, content = page.parse_yaml_md(f"{config.content_dir_page}/{slug}.md")
    data['html'] = markdown.markdown(content, extensions=[
        'markdown.extensions.extra',
        'markdown.extensions.codehilite',
        'markdown.extensions.toc'
    ], extension_configs={"markdown.extensions.toc": {"anchorlink": True}})
    data['html'] = page.replace(data['html'])
    return template.render_template('page.html', **data)
