title: "Sway, SWMO: missing seat group"
date: 2021-10-12
---

With
[latest sway and seatd upgrades in edge](https://todo.sr.ht/~mil/sxmo-tickets/394#event-102183),
the user must be in the "seat" group, or else sway will not start:

<small>
<pre>
[ERROR] [wlr] [libseat] [libseat/backend/seatd.c:82] Could not connect to socket /run/seatd.sock: Permission denied
[ERROR] [wlr] [libseat] [libseat/backend/logind.c:653] Could not get primary session for user: No data available
[ERROR] [wlr] [libseat] [libseat/libseat.c:79] No backend was able to open a seat
[ERROR] [wlr] [backend/session/session.c:84] Unable to create seat: Function not implemented
[ERROR] [wlr] [backend/session/session.c:218] Failed to load session backend
[ERROR] [wlr] [backend/backend.c:353] Failed to start a DRM session
[ERROR] [sway/server.c:53] Unable to create backend
</pre>
</small>

With [pma!2631](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2631),
it will be fixed for new installations. Existing installations need to
add the user to the group manually.

<pre>
$ sudo usermod -G seat -a $USER
</pre>

Related: [sxmo-tickets#394](https://todo.sr.ht/~mil/sxmo-tickets/394)
