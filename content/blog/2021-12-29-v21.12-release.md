title: "postmarketOS Release: v21.12"
title-short: "v21.12 Release"
date: 2021-12-29
preview: "2021-12/v21.12_pinebookpro.jpg"
---

[![](/static/img/2021-12/v21.12_pinebookpro.jpg){: class="wfull border" }](/static/img/2021-12/v21.12_pinebookpro.jpg)

It's the end of another year, and we're happy to announce the v21.12 release of
postmarketOS, based on Alpine Linux 3.15!

Thanks to our amazing contributors, the amount of supported devices has been
increased to 23 (from 15 in v21.06). As in previous releases, we ship pre-built
images for phones and tablets with the mobile-optimized Phosh, Plasma Mobile
and Sxmo interfaces. Other form factors, such as the PineBook Pro get
images with different interfaces as it makes sense. In the picture above it
runs Plasma Desktop, and besides that we have Gnome, Sway, Phosh and Console.
Find all images at the [download](/download/) page.

v21.12, like previous releases, is geared mainly towards Linux
enthusiasts; it may be a bit rough around the edges so expect some bugs. Help
identifying and resolving issues is always greatly appreciated.

## Supported devices

[#grid side#]

[![](/static/img/2021-12/op6_thumb.jpg){: class="w300 border" }](/static/img/2021-12/op6.jpg)
<br><span class="w300">
The OnePlus 6, enjoying a fun day in nature with mainline Linux and Phosh. pmOS
tweaks was used to deal with the clock and notch.
</span>

[#grid text#]

* Arrow DragonBoard 410c <span class="new">new</span>
* ASUS MeMo Pad 7
* BQ Aquaris X5
* Lenovo A6000 <span class="new">new</span>
* Lenovo A6010 <span class="new">new</span>
* Motorola Moto G4 Play
* ODROID HC2 <span class="new">new</span>
* OnePlus 6
* OnePlus 6T
* PINE64 PineBook Pro <span class="new">new</span>
* PINE64 PinePhone
* PINE64 PineTab
* PINE64 RockPro64 <span class="new">new</span>
* Purism Librem 5
* Samsung Galaxy A3 (2015)
* Samsung Galaxy A5 (2015)
* Samsung Galaxy S4 Mini Value Edition
* Samsung Galaxy Tab A 8.0 <span class="new">new</span>
* Samsung Galaxy Tab A 9.7 <span class="new">new</span>
* Wileyfox Swift
* Xiaomi Mi Note 2
* Xiaomi Pocophone F1 <span class="new">new</span>
* Xiaomi Redmi 2

[#grid end#]

[#grid side#]

[![](/static/img/2021-12/v21.12_phosh_thumb.jpg){: class="w300 border" }](/static/img/2021-12/v21.12_phosh.jpg)
<br><span class="w300">
The combined application and task switcher of Phosh 0.14.0 with Firefox and
Portfolio.
</span>

[#grid text#]

## Highlights

### User Interfaces

* [Sxmo 1.6.x](https://lists.sr.ht/~mil/sxmo-announce/%3C31IGOQIT71NT5.2LYA6VY1SCLPK%40stacyharper.net%3E)
  brings the whole Sxmo stack from X11 to Wayland! The Sxmo development team
  and users tested this impressive undertaking in postmarketOS edge since
  November, and after the usual release coordination we're happy to have 1.6.1
  running on top of this new stable pmOS v21.12 release.

* Plasma Mobile Gear 21.12 not only has more or less by coincidence the same
  release version as this postmarketOS release. But also it brings a huge
  amount of improvements over the last version. Find some great visual
  impressions over at the
  [Plasma Mobile Blog](https://plasma-mobile.org/2021/12/07/plasma-mobile-gear-21-12/).

* Phosh 0.14.0 is the same version we backported in last month's
  [v21.06 SP4](/blog/2021/11/08/v21.06.4-release/). However some small tweaks
  have been made (such as properly displaying the Firefox icon in the preview
  window, as the screenshot shows), and many of the GNOME apps such as
  gnome-settings have been upgraded to their GNOME 41 versions.



[#grid end#]
[#grid side#]
[![](/static/img/2021-12/v21.12_ttyescape_thumb.jpg){: class="w300 border" }](/static/img/2021-12/v21.12_ttyescape.jpg)
[#grid text#]

### TTYescape
Sometimes, unfortunately, things go sideways and the UI locks up or crashes. On
a system with a physical keyboard, you can easily jump to a new TTY to poke
around, restart things, or even just cleanly reboot. This is much more
difficult on a device with no physical keyboard... until now! TTYescape is an
amazing tool that uses hardware keys to quickly send you to a second TTY and
launch a touch keyboard so you can easily recover from situations that would
have required a force reboot in the past. Make sure you remember the key
combination:

* press the volume down button (keep it pressed)
* tap the power button three times
* release the volume down button

Run the same steps again to get back to your UI. Starting with postmarketOS
v21.12, this works with Sxmo, Phosh and Plasma Mobile. Read more about it in
the [wiki page](https://wiki.postmarketos.org/wiki/TTYescape).

[#grid end#]

[#grid side#]
[![](/static/img/2021-12/v21.12_tweaks.jpg){: class="w300 border" }](/static/img/2021-12/v21.12_tweaks.jpg)
[#grid text#]
### postmarketos-tweaks 0.9.0

Tweaks 0.9.0 is now able to control the
[app drawer filter](https://linmob.net/phosh-0-12-app-drawer/) in Phosh. On a
related note, postmarketos-tweaks has been able for a long time to set the
suspend timeout for phones that can go into deep sleep (notably the PinePhone).
With postmarketOS v21.12 we set the default value from 15 minutes to two
minutes, which saves a lot of battery.


### boot-deploy / postmarketos-mkinitfs >= 1.0.0

The tool that generates boot files (postmarketos-mkinitfs) was rewritten and
supporting scripts for installing boot-related files (boot-deploy) were
improved to help with overall reliability when updating the kernel and
initramfs. Files are installed atomically, so having an unbootable device after
an update should be a thing of the past.

[#grid end#]

### mobile-config-firefox 3.0.0

Firefox >= 91 had several design changes and required parts of
mobile-config-firefox to be rewritten. While at it, the navigation bar was
moved to the bottom after asking the community in a poll whether it's a good
idea or not and a whole bunch of UI glitches were fixed. u-block origin is also
installed by default now. Details and photos in
[this thread](https://fosstodon.org/@ollieparanoid/107394745970284867).



[#grid text#]
## Upgrading from v21.06

If you have the previous release, postmarketOS v21.06, installed on your
device, the method described for upgrading previous releases is still the best
option. You can find detailed instructions on [the wiki page for upgrading
existing installations of
postmarketOS](https://wiki.postmarketos.org/wiki/Upgrade_release). We
understand this is not the best solution, and are working to provide a more
streamlined upgrade experience in a future postmarketOS release.
[#grid side#]
[![](/static/img/2021-12/v21.12_mi_note2_thumb.jpg){: class="w300 border" }](/static/img/2021-12/v21.12_mi_note2.jpg)
<br><span class="w300">
Xiaomi Mi Note 2 displaying the welcome app on a sunny day below a palm tree.
</span>
[#grid end#]

### Nokia N900 didn't make it this time

[#grid side#]

[![](/static/img/2021-03/n900-i3wm-thumb.jpg){: class="w300 border" }](/static/img/2021-03/n900-i3wm.jpg)
<br><span class="w300">
Conky has been displaying the 5.7 kernel for too long in the pmOS port of the
Nokia N900.
</span>

[#grid text#]

If you read the above list carefully and have been following postmarketOS for
a while, you probably noticed that the N900 isn't in the list anymore. Despite
the fact that it is a beloved classic device (we even celebrated its
[10th birthday](/blog/2019/11/01/ten-years-of-nokia-n900/) here on the blog),
most people contributing to postmarketOS are focusing on other devices.

After a [last call](https://fosstodon.org/@postmarketOS/107309633255683213) we
had to drop it from the release and will soon move it back from the "community"
[device category](https://wiki.postmarketos.org/wiki/Device_categorization)
back to "testing". In the near future, there won't be new pre-built
postmarketOS images for the N900 available anymore, however you can still
run pmOS edge on it if you do the
[installation with pmbootstrap](https://wiki.postmarketos.org/wiki/Installation_guide).
Just be aware that there most likely won't be any upgrades to the kernel etc.
unless you contribute them yourself.
[#grid end#]

Another option for N900 users is checking out the port from our friends at
[Maemo Leste](https://leste.maemo.org/Nokia_N900), which is still actively
maintained.


## More development news

The new release includes all but the most recent improvements that were made to
postmarketOS edge. Check out the
[postmarketOS podcast](https://cast.postmarketos.org/) where we discuss these
in detail.

## Comments

* [Mastodon](https://fosstodon.org/web/@postmarketOS/107531914067276629)
* [Lemmy](https://lemmy.ml/post/133011)
<small>
* [Reddit](https://www.reddit.com/r/postmarketOS/comments/rrgxl5/postmarketos_release_v2112/)
* [Twitter](https://twitter.com/postmarketOS/status/1476277085866016772)
* [HN](https://news.ycombinator.com/item?id=29729146)
</small>
